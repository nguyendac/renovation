<div class="bx_tel">
  <p class="show_pc"><img src="/asset/img/txt_tel.png" alt="Tel"></p>
  <div class="show_sp">
    <p class="txt_tel">
      <span class="txt_tel_f"><img src="/asset/img/txt_tel_01.png" alt="Tel 01"></span>
      <a class="tel" href="tel:0120-453-553"><img src="/asset/img/tel.png" alt="NB tel"></a>
      <span class="txt_tel_s"><img src="/asset/img/txt_tel_02.png" alt="Tel 02"></span>
    </p>
    <em><img src="../asset/img/txt_tel_03.png" alt="Tel 03"></em>
  </div>
</div>
<!--/.bx_tel-->