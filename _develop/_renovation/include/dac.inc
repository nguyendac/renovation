<nav class="reno_ctl row">
  <ul class="reno_ctl_l" id="reno_ctl">
    <li class="anchor" href="#family">
      <div class="reno_ctl_l_t">
        <p><img src="./asset/img/reno_pc.png" alt="renovation story" class="resimg"></p>
        <h3><img src="./asset/img/reno_01_ctl.png" alt=""></h3>
      </div>
      <figure><img src="./asset/img/reno_01_img_01.jpg" alt=""></figure>
    </li>
    <li class="anchor" href="#single">
      <div class="reno_ctl_l_t">
        <p><img src="./asset/img/reno_pc.png" alt="renovation story" class="resimg"></p>
        <h3><img src="./asset/img/reno_02_ctl.png" alt=""></h3>
      </div>
      <figure><img src="./asset/img/reno_02_img_01.jpg" alt=""></figure>
    </li>
    <li class="anchor" href="#dinks">
      <div class="reno_ctl_l_t">
        <p><img src="./asset/img/reno_pc.png" alt="renovation story" class="resimg"></p>
        <h3><img src="./asset/img/reno_03_ctl.png" alt=""></h3>
      </div>
      <figure><img src="./asset/img/reno_03_img_01.jpg" alt=""></figure>
    </li>
  </ul>
  <div class="reno_ctl_btn show_sp">
    <button class="reno_ctl_btn_prev reno_ctl_btn_c" id="reno_ctl_btn_prev">prev</button>
    <button class="reno_ctl_btn_next reno_ctl_btn_c" id="reno_ctl_btn_next">next</button>
  </div>
</nav>
<section id="family" class="reno_family reno_sec row">
  <div class="reno_sec_t reno_family_t">
    <div class="reno_sec_t_l reno_family_l">
      <span><img src="./asset/img/reno_pc.png" alt="" class="resimg"></span>
      <h2><img src="./asset/img/reno_01_txt_01_pc.png" alt="" class="resimg"></h2>
      <p><img src="./asset/img/reno_01_txt_02_pc.png" alt="" class="resimg"></p>
    </div>
    <figure><img src="./asset/img/reno_01_img_01.jpg" alt=""></figure>
  </div>
  <div class="reno_sec_m1 effect fadeIn delay_06">
    <h3><em><img src="./asset/img/reno_01_txt_03_pc.png" alt="" class="resimg"></em><span><img src="./asset/img/reno_01_txt_04_pc.png" alt="" class="resimg"></span></h3>
    <div class="reno_sec_m1_f">
      <figure><img src="./asset/img/reno_01_img_02.jpg" alt=""></figure>
      <figure><img src="./asset/img/reno_01_img_03.jpg" alt=""></figure>
    </div>
    <p><img src="./asset/img/reno_01_txt_05_pc.png" alt="" class="resimg"></p>
  </div>
  <div class="reno_sec_m2 reno_family_m2 effect fadeIn delay_06">
    <h3><img src="./asset/img/reno_01_txt_06_pc.png" alt="" class="resimg"></h3>
    <div class="reno_sec_m2_wrap reno_family_m2_wrap">
      <article>
        <div class="reno_sec_m2_wrap_t reno_family_m2_wrap_t">
          <h4><img src="./asset/img/reno_01_ex_01_pc.png" alt="" class="resimg"></h4>
          <p><img src="./asset/img/reno_01_txt_07_pc.png" alt="" class="resimg"></p>
        </div>
        <figure><img src="./asset/img/reno_01_img_04.jpg" alt=""></figure>
      </article>
      <article>
        <div class="reno_sec_m2_wrap_t reno_family_m2_wrap_t">
          <h4><img src="./asset/img/reno_01_ex_02_pc.png" alt="" class="resimg"></h4>
          <p><img src="./asset/img/reno_01_txt_08_pc.png" alt="" class="resimg"></p>
        </div>
        <figure><img src="./asset/img/reno_01_img_05.jpg" alt=""></figure>
      </article>
      <article>
        <div class="reno_sec_m2_wrap_t reno_family_m2_wrap_t">
          <h4><img src="./asset/img/reno_01_ex_03_pc.png" alt="" class="resimg"></h4>
          <p><img src="./asset/img/reno_01_txt_09_pc.png" alt="" class="resimg"></p>
        </div>
        <figure><img src="./asset/img/reno_01_img_06.jpg" alt=""></figure>
      </article>
    </div>
  </div>
  <div class="reno_sec_b effect fadeIn delay_06">
    <h3><img src="./asset/img/reno_01_txt_10_pc.png" alt="" class="resimg"></h3>
    <figure>
      <img src="./asset/img/reno_01_img_07.jpg" alt="">
      <figcaption><img src="./asset/img/reno_01_txt_11_pc.png" alt="" class="resimg"></figcaption>
    </figure>
  </div>
</section><!-- end reno family -->
<div class="reno_yellow">
  <!--#include virtual="/include/tel.inc"-->
  <!--#include virtual="/include/yellow.inc"-->
</div>
<section id="single" class="reno_single reno_sec row">
  <div class="reno_sec_t reno_single_t">
    <div class="reno_sec_t_l reno_single_l">
      <span><img src="./asset/img/reno_pc.png" alt="" class="resimg"></span>
      <h2><img src="./asset/img/reno_02_txt_01_pc.png" alt="" class="resimg"></h2>
      <p><img src="./asset/img/reno_02_txt_02_pc.png" alt="" class="resimg"></p>
    </div>
    <figure><img src="./asset/img/reno_02_img_01.jpg" alt=""></figure>
  </div>
  <div class="reno_sec_m1 effect fadeIn delay_06">
    <h3><em><img src="./asset/img/reno_01_txt_03_pc.png" alt="" class="resimg"></em><span><img src="./asset/img/reno_02_txt_04_pc.png" alt="" class="resimg"></span></h3>
    <div class="reno_sec_m1_f">
      <figure><img src="./asset/img/reno_02_img_02.jpg" alt=""></figure>
      <figure><img src="./asset/img/reno_02_img_03.jpg" alt=""></figure>
    </div>
    <p><img src="./asset/img/reno_02_txt_05_pc.png" alt="" class="resimg"></p>
  </div>
  <div class="reno_sec_m2 reno_single_m2 effect fadeIn delay_06">
    <h3><img src="./asset/img/reno_01_txt_06_pc.png" alt="" class="resimg"></h3>
    <div class="reno_sec_m2_wrap reno_single_m2_wrap">
      <article>
        <div class="reno_sec_m2_wrap_t reno_single_m2_wrap_t">
          <h4><img src="./asset/img/reno_02_ex_01_pc.png" alt="" class="resimg"></h4>
          <p><img src="./asset/img/reno_02_txt_07_pc.png" alt="" class="resimg"></p>
        </div>
        <figure><img src="./asset/img/reno_02_img_04.jpg" alt=""></figure>
      </article>
      <article>
        <div class="reno_sec_m2_wrap_t reno_single_m2_wrap_t">
          <h4><img src="./asset/img/reno_02_ex_02_pc.png" alt="" class="resimg"></h4>
          <p><img src="./asset/img/reno_02_txt_08_pc.png" alt="" class="resimg"></p>
        </div>
        <figure><img src="./asset/img/reno_02_img_05.jpg" alt=""></figure>
      </article>
      <article>
        <div class="reno_sec_m2_wrap_t reno_single_m2_wrap_t">
          <h4><img src="./asset/img/reno_02_ex_03_pc.png" alt="" class="resimg"></h4>
          <p><img src="./asset/img/reno_02_txt_09_pc.png" alt="" class="resimg"></p>
        </div>
        <figure><img src="./asset/img/reno_02_img_06.jpg" alt=""></figure>
      </article>
    </div>
  </div>
  <div class="reno_sec_b effect fadeIn delay_06">
    <h3><img src="./asset/img/reno_01_txt_10_pc.png" alt="" class="resimg"></h3>
    <figure>
      <img src="./asset/img/reno_02_img_07.jpg" alt="">
      <figcaption><img src="./asset/img/reno_02_txt_11_pc.png" alt="" class="resimg"></figcaption>
    </figure>
  </div>
</section><!-- end single -->
<div class="reno_yellow">
  <!--#include virtual="/include/tel.inc"-->
  <!--#include virtual="/include/yellow.inc"-->
</div>
<section id="dinks" class="reno_dink reno_sec row">
  <div class="reno_sec_t reno_dink_t">
    <div class="reno_sec_t_l reno_dink_l">
      <span><img src="./asset/img/reno_pc.png" alt="" class="resimg"></span>
      <h2><img src="./asset/img/reno_03_txt_01_pc.png" alt="" class="resimg"></h2>
      <p><img src="./asset/img/reno_03_txt_02_pc.png" alt="" class="resimg"></p>
    </div>
    <figure><img src="./asset/img/reno_03_img_01.jpg" alt=""></figure>
  </div>
  <div class="reno_sec_m1 effect fadeIn delay_06">
    <h3><em><img src="./asset/img/reno_01_txt_03_pc.png" alt="" class="resimg"></em><span><img src="./asset/img/reno_03_txt_04_pc.png" alt="" class="resimg"></span></h3>
    <div class="reno_sec_m1_f">
      <figure><img src="./asset/img/reno_03_img_02.jpg" alt=""></figure>
      <figure><img src="./asset/img/reno_03_img_03.jpg" alt=""></figure>
    </div>
    <p><img src="./asset/img/reno_03_txt_05_pc.png" alt="" class="resimg"></p>
  </div>
  <div class="reno_sec_m2 reno_dink_m2 effect fadeIn delay_06">
    <h3><img src="./asset/img/reno_01_txt_06_pc.png" alt="" class="resimg"></h3>
    <div class="reno_sec_m2_wrap reno_dink_m2_wrap">
      <article>
        <div class="reno_sec_m2_wrap_t reno_dink_m2_wrap_t">
          <h4><img src="./asset/img/reno_03_ex_01_pc.png" alt="" class="resimg"></h4>
          <p><img src="./asset/img/reno_03_txt_07_pc.png" alt="" class="resimg"></p>
        </div>
        <figure><img src="./asset/img/reno_03_img_04.jpg" alt=""></figure>
      </article>
      <article>
        <div class="reno_sec_m2_wrap_t reno_dink_m2_wrap_t">
          <h4><img src="./asset/img/reno_03_ex_02_pc.png" alt="" class="resimg"></h4>
          <p><img src="./asset/img/reno_03_txt_08_pc.png" alt="" class="resimg"></p>
        </div>
        <figure><img src="./asset/img/reno_03_img_05.jpg" alt=""></figure>
      </article>
      <article>
        <div class="reno_sec_m2_wrap_t reno_dink_m2_wrap_t">
          <h4><img src="./asset/img/reno_03_ex_03_pc.png" alt="" class="resimg"></h4>
          <p><img src="./asset/img/reno_03_txt_09_pc.png" alt="" class="resimg"></p>
        </div>
        <figure><img src="./asset/img/reno_03_img_06.jpg" alt=""></figure>
      </article>
    </div>
  </div>
  <div class="reno_sec_b effect fadeIn delay_06">
    <h3><img src="./asset/img/reno_01_txt_10_pc.png" alt="" class="resimg"></h3>
    <figure>
      <img src="./asset/img/reno_03_img_07.jpg" alt="">
      <figcaption><img src="./asset/img/reno_03_txt_11_pc.png" alt="" class="resimg"></figcaption>
    </figure>
  </div>
</section><!-- end single -->
<div class="reno_yellow">
  <!--#include virtual="/include/tel.inc"-->
  <!--#include virtual="/include/yellow.inc"-->
</div>
