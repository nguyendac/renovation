<div class="bx_yl">
  <div class="bx_yl_inner">
    <h3><img src="/asset/img/h3_yl_pc.png" class="resimg" alt="Title yellow"></h3>
    <div class="bx_yl_inner_amz">
      <div class="bx_yl_inner_amz_left">
        <img src="/asset/img/txt_left_yl_pc.png" class="resimg" alt="Text Left">
      </div>
      <!--/.left-->
      <figure><img src="/asset/img/img_amz_pc.png" class="resimg" alt="Images Amazon"></figure>
    </div>
    <!--/.bx_yl_amz--> 
    <div class="btn">
      <a href="#">
        <img src="/asset/img/btn_mail_pc.png" class="resimg" alt="Button Mail">
      </a>
    </div>
  </div>
  <!--/.inner-->
</div>
<!--/.bx_yl-->