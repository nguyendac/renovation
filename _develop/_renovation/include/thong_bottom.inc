<div class="ctn_bottom">
  <div class="ctn_bottom_inner">
    <div class="bx_art_top">
      <article class="effect fadeIn delay_06">
        <div class="left">
          <h3 class="ttl_art"><img src="/asset/img/tt_art_01_pc.png" class="resimg" alt="Title Article 01"></h3>
          <p>nuリノベーションのHPには約600件の施工事例を掲載しております。お客様の想いや価値観を丁寧にヒアリングし、フルオーダーにて設計・デザインするのがnuリノベーションの特徴です。どれ1つとして同じデザインのない、自分らしい暮らしが手に入ります。</p>
        </div>
        <!--/.left-->
        <figure><img src="/asset/img/img_01.png" alt="Images 01"></figure>
      </article>
      <article  class="effect fadeIn delay_06">
        <div class="left">
          <h3 class="ttl_art"><img src="/asset/img/tt_art_02_pc.png" class="resimg" alt="Title Article 02"></h3>
          <ul class="left_l">
            <li>
              <span>雑誌：</span>
              <em>InRed、VERY、steady、SPRiNG、男の隠れ家、<br class="show_pc">GO OUT、Hanako、relife＋、</em>
            </li>
            <li>
              <span>TV：</span>
              <em>BSフジ「デザインリフォーム 〜Design ReHOME〜」 <br class="show_pc">フジテレビ「にじいろジーン」<br>日本テレビ「スッキリ」</em>
            </li>
          </ul>
        </div>
        <!--/.left-->
        <figure><img src="/asset/img/img_02.png" alt="Images 02"></figure>
      </article>
    </div>
    <!--/.bx_art_top-->
    <div class="bx_strength">
      <h3>nuの3つの強み</h3>
      <p class="txt_top">nuリノベーションでワンストップリノベーションを<br class="show_sp">する際の3つの強みをまとめました。</p>
      <ul class="bx_strength_list_nb">
        <li>
          <figure>
            <img src="/asset/img/nb_01.png" alt="NB 01">
          </figure>
          <em>100%オーダー型<br>リノベーション</em>
        </li>
        <li>
          <figure>
            <img src="/asset/img/nb_02.png" alt="NB 02">
          </figure>
          <em>ヒアリングを重視した<br>高いデザイン力</em>
        </li>
        <li>
          <figure>
            <img src="/asset/img/nb_03.png" alt="NB 03">
          </figure>
          <em>高いリノベーション<br>クオリティとオリジナリティ</em>
        </li>
      </ul>
      <!--/.bx_strength_list_nb-->
      <article class="effect fadeIn delay_06">
        <h4>
          <figure>
            <img src="/asset/img/nb_01.png" alt="01">
          </figure>
          <em>100%オーダー型リノベーション</em>
        </h4>
        <p>
          「リノベーション」は、大きく分けて3つのカテゴリーに分けられます。<br>
          1.「リノベーション済みマンション」2.「パッケージ型定額制リノベーション」3.「100%オーダー型リノベーション」。<br>
          nuリノベーションで提供しているのは、100%オーダー型リノベーションです。間取りはもちろん、設備や素材に至るまで、全てゼロから創り上げる<br>
          世界に1つだけのお客様の空間が完成します。<br>
          時間や手間はかかりますが、その分完成した時の達成感や満足感は他には変えられないものがあります。
        </p>
      </article>
      <article class="effect fadeIn delay_06">
        <h4>
          <figure>
            <img src="/asset/img/nb_02.png" alt="02">
          </figure>
          <em>ヒアリングを重視した<br class="show_sp">高いデザイン力</em>
        </h4>
        <p>
          nuリノベーションでは、設計デザインの打合せをスタートさせる前に必ず「ヒアリング」のお時間をお取りしています。
          どんなデザインが好きか、どんな間取りの家に住みたいか…そういったこともお聞きするのはもちろんですが、それよりも「1日でより多くの時間を過ごす場所」や「家族全員の利き手」、それに「起床時間とよく食べる朝食」などを細かくお聞きしております。
          それは実際にリノベーションする際にとても大切なことで、ご入居後の住み心地や使いやすさにとても影響するポイントなのです。
          お客様のライフスタイルについて、ぜひ設計デザイナーに余すことなく共有してください。約2時間、長い方では3時間ほどのお時間がかかりますが、イメージの共有にブレをなくすことでお客様の価値観により合った空間が完成します。
        </p>
      </article>
      <article class="effect fadeIn delay_06">
        <h4>
          <figure>
            <img src="/asset/img/nb_03.png" alt="03">
          </figure>
          <em>高いリノベーションクオリティ<br class="show_sp">とオリジナリティ</em>
        </h4>
        <p>
          nuリノベーションのスタッフは自身の担当領域だけでなく、物件探しから設計・施工、アフターサービスまで全てに精通しているリノベーションのプロフェッショナルです。
          そのためいつも窓口は1つ。どんな質問でもお答えしますので、いつも安心して家づくりを楽しんでいただけます。また、
          nuクオリティという高い施工技術を社内の基準としているためお客様に満足していただける家づくりを心掛けております。
          また、施工中にお客様参加型のオリジナリティを追求できるようなサービスもご提案しております。完成後の達成感を一緒に味わいましょう。
        </p>
      </article>
    </div>
    <!--/.bx_strength-->
  </div>
  <!--/.ctn_bottom_inner-->
  <!--#include virtual="/include/yellow.inc"-->
  <!--#include virtual="/include/tel.inc"-->
</div>
<!--/.ctn_bottom-->