window.requestAnimFrame = (function() {
  return window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(callback) {
      window.setTimeout(callback, 1000 / 60);
    };
})();
window.cancelAnimFrame = (function() {
  return window.cancelAnimationFrame ||
    window.cancelRequestAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    function(id) { window.clearTimeout(id); };
})();

function closest(el, selector) {
  // type el -> Object
  // type select -> String
  var matchesFn;
  // find vendor prefix
  ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function(fn) {
    if (typeof document.body[fn] == 'function') {
      matchesFn = fn;
      return true;
    }
    return false;
  })
  var parent;
  // traverse parents
  while (el) {
    parent = el.parentElement;
    if (parent && parent[matchesFn](selector)) {
      return parent;
    }
    el = parent;
  }
  return null;
}

function getCssProperty(elem, property) {
  return window.getComputedStyle(elem, null).getPropertyValue(property);
}
var easingEquations = {
  easeOutSine: function(pos) {
    return Math.sin(pos * (Math.PI / 2));
  },
  easeInOutSine: function(pos) {
    return (-0.5 * (Math.cos(Math.PI * pos) - 1));
  },
  easeInOutQuint: function(pos) {
    if ((pos /= 0.5) < 1) {
      return 0.5 * Math.pow(pos, 5);
    }
    return 0.5 * (Math.pow((pos - 2), 5) + 2);
  }
};

function isPartiallyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;
  return ((top + height >= 0) && (height + window.innerHeight >= bottom));
}

function isFullyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  return ((top >= 0) && (bottom <= window.innerHeight));
}
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
function getPosition(el) {
  var xPos = 0;
  var yPos = 0;
  while (el) {
    if (el.tagName == "BODY") {
      // deal with browser quirks with body/window/document and page scroll
      var xScroll = el.scrollLeft || document.documentElement.scrollLeft;
      var yScroll = el.scrollTop || document.documentElement.scrollTop;

      xPos += (el.offsetLeft - xScroll + el.clientLeft);
      yPos += (el.offsetTop - yScroll + el.clientTop);
    } else {
      // for all other non-BODY elements
      xPos += (el.offsetLeft - el.scrollLeft + el.clientLeft);
      yPos += (el.offsetTop - el.scrollTop + el.clientTop);
    }
    el = el.offsetParent;
  }
  return {
    x: xPos,
    y: yPos
  };
}
var transform = ["transform", "msTransform", "webkitTransform", "mozTransform", "oTransform"];
var flex = ['-webkit-box', '-moz-box', '-ms-flexbox', '-webkit-flex', 'flex'];
var fd = ['flexDirection', '-webkit-flexDirection', '-moz-flexDirection'];

function getSupportedPropertyName(properties) {
  for (var i = 0; i < properties.length; i++) {
    if (typeof document.body.style[properties[i]] != "undefined") {
      return properties[i];
    }
  }
  return null;
}
var transformProperty = getSupportedPropertyName(transform);
var flexProperty = getSupportedPropertyName(flex);
var fdProperty = getSupportedPropertyName(fd);
var Effect = (function(){
  function Effect(){
    var e = this;
    this.eles = document.querySelectorAll('.effect');
    this.handling = function(){
      var _top  = document.documentElement.scrollTop
      Array.prototype.forEach.call(e.eles,function(el,i){
        if(isPartiallyVisible(el)){
          el.classList.add('active');
        }
      })
    }
    window.addEventListener('scroll',e.handling,false);
    this.handling();
  }
  return Effect;
})()
var Anchor = (function(){
  function Anchor(){
    var a = this;
    this._target = '.anchor';
    this.timer;
    this.flag_start = false;
    this.iteration;
    this.eles = document.querySelectorAll(this._target);
    this.stopEverything = function(){
      a.flag_start = false;
    }
    this._getbuffer = function() {
      var _buffer;
      _buffer = 0;
      return _buffer;
    }
    this._buffer = this._getbuffer();
    this.scrollToY = function(scrollTargetY,speed,easing){
      var scrollY = window.scrollY || window.pageYOffset,
        scrollTargetY = scrollTargetY || 0,
        speed = speed || 2000,
        easing = easing || 'easeOutSine',
        currentTime = 0;
      var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));
      function tick() {
        if(a.flag_start){
          currentTime += 1 / 60;
          var p = currentTime / time;
          var t = easingEquations[easing](p);
          if (p < 1) {
            requestAnimFrame(tick);
            window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
          } else {
            window.scrollTo(0, scrollTargetY);
          }
        }
      }
      tick();
    }
    Array.prototype.forEach.call(this.eles,function(el,i){
      el.addEventListener('click',function(e){
        var next = el.getAttribute('href').split('#')[1];
        if(document.getElementById(next)){
          a.flag_start = true;
          e.preventDefault();
          a.scrollToY((document.getElementById(next).offsetTop-a._buffer),1500,'easeOutSine');
        }
      })
    });
    this._start = function(){
      var next = window.location.hash.split('#')[1];
      a.flag_start = true;
      if(next){
        a.scrollToY((document.getElementById(next).offsetTop - a._buffer),1500,'easeOutSine');
      }
    }
    window.addEventListener('load',a._start,false);
    document.querySelector("body").addEventListener('mousewheel',a.stopEverything,false);
    document.querySelector("body").addEventListener('DOMMouseScroll',a.stopEverything,false);
  }
  return Anchor;
})()
// var Slide = (function(){
//   function Slide(){
//     var s = this;
//     this.target = 'reno_ctl';
//     this.step  = 0.0005;
//     this.scale = 1;
//     this.currentSlide = 0;
//     this.opacity = 0;
//     this.timer;
//     this.timeout;
//     this.img;
//     this.eles = document.getElementById(this.target).querySelectorAll('li');
//     this.btnPrev = document.getElementById('reno_ctl_btn_prev');
//     this.btnNext = document.getElementById('reno_ctl_btn_next');
//     this.func_transition = function(){
//       s.img = s.eles[s.currentSlide];
//       s.img.classList.add('open');
//       Array.prototype.forEach.call(s.eles,function(el,k){
//         if(k != s.currentSlide) {
//           el.classList.remove('open');
//         }
//       })
//       s.scale+=s.step;
//       if(s.scale >= 1.15){
//         s.currentSlide+=1;
//         s.img.classList.remove('open');
//         s.scale = 1;
//         if(s.currentSlide > s.eles.length-1) {
//           s.currentSlide = 0;
//         }
//       }
//       s.timer = window.requestAnimFrame(s.func_transition);
//     }
//     /** Prev Slider **/
//     this.btnPrev.addEventListener('click', function() {
//       cancelAnimFrame(s.timer);
//       s.scale = 1;
//       s.img = s.eles[s.currentSlide];
//       s.img.classList.remove('open');
//       s.currentSlide -= 1;
//       if (s.currentSlide < 0) {
//         s.currentSlide = s.eles.length - 1
//       }
//       s.func_transition();
//     });
//     /** Next Slider **/
//     this.btnNext.addEventListener('click', function() {
//       cancelAnimFrame(s.timer);
//       s.scale = 1;
//       s.img = s.eles[s.currentSlide];
//       s.img.classList.remove('open');
//       s.currentSlide += 1;
//       if (s.currentSlide > s.eles.length - 1) {
//         s.currentSlide = 0;
//       }
//       s.func_transition();
//     });
//     window.addEventListener('resize',function(){
//       //s.sizeWindow();
//     })
//     this.func_transition();
//   }
//   return Slide;
// })()
var Slide = (function(){
  function Slide(){
    var s = this;
    this.target = document.getElementById('slider');
    this.container = this.target.querySelector('.main_slider ul');
    this.lies = this.container.querySelectorAll('li');
    this.stepwidth = this.lies[0].clientWidth;
    this._length = this.lies.length;
    this.current = 0;
    this.sd = 0;
    this.step = this.stepwidth/60;
    this.delay;
    this.action = false;
    this.reset = function(){
      s.stepwidth = s.lies[0].clientWidth;
      s.step = s.stepwidth/60;
    }
    this.pos = function() {
      Array.prototype.forEach.call(s.lies,function(el,k){
        if(k>=s.current) {
          if(k == s.current) {
            s.lies[k].style.left = 0;
            s.lies[k].classList.add('open')
          } else {
            s.lies[k].style.left = s.stepwidth+'px';
            s.lies[k].classList.remove('open')
          }
        } else {
          s.lies[k].style.left = -s.stepwidth+'px';
          s.lies[k].classList.remove('open')
        }
      })
    }

    this._delay = function() {
      if (s.sd < 300) {
        s.sd += 1;
        s.delay = window.requestAnimFrame(s._delay);
      } else {
        window.cancelAnimFrame(s.delay);
        s.sd = 0;
        s.handling_next();
      }
    }
    this.handling_next = function(nt){
      var next = (nt!=null) ? nt: (parseInt(s.current)+1);
      var cur = s.current;
      if(next > s._length - 1) {
        next = 0;
        if(cur == s._length - 1) {
          s.handling_prev(0);
          return;
        }
      }
      if(next < 0) {
        next = s._length - 1;
      }
      s.lies[next].classList.add('open')
      var lc = 0;
      var ln = parseInt(getCssProperty(s.lies[next],'left'));
      var timer;
      function tick() {
        if(lc >= -s.stepwidth) {
          s.action = true;
          timer = window.requestAnimFrame(tick);
          lc-=s.step;
          ln-=s.step;
          s.lies[cur].style.left = lc+'px';
          if(ln <= 0) {
            ln = 0;
          }
          s.lies[next].style.left = ln+'px';
        } else {
          s.current = next;
          s.pos();
          window.cancelAnimFrame(timer);
          s.action = false;
          s.delay = window.requestAnimFrame(s._delay);
        }
      }
      tick();
    }
    this.handling_prev = function(nt) {
      var prev = (nt!=null) ? nt: (parseInt(s.current)-1);
      var cur = s.current;
      if(prev < 0) {
        prev = s._length - 1;
      }
      s.lies[prev].classList.add('open')
      var lc = 0;
      var lp = parseInt(getCssProperty(s.lies[prev],'left'));
      var timer;
      function tick(){
        if(lc < s.stepwidth) {
          s.action = true;
          timer = window.requestAnimFrame(tick);
          lc+=s.step;
          lp+=s.step;
          s.lies[cur].style.left = lc+'px';
          if(lp >= 0) {
            lp = 0;
          }
          s.lies[prev].style.left = lp+'px';
        } else {
          s.current = prev;
          s.pos();
          window.cancelAnimFrame(timer);
          s.action = false;
          s.delay = window.requestAnimFrame(s._delay);
        }
      }
      tick();
    }
    window.addEventListener('load',function(){
      s.reset();
      s.pos();
    })
    this.rtime;
    this.timeout = false;
    this.delta = 200;
    this.timer;
    this.resizeend = function(){
      if (new Date() - s.rtime < s.delta) {
        window.cancelAnimFrame(s.delay);
        setTimeout(s.resizeend, s.delta);
      } else {
        s.timeout = false;
        s.action = false;
        s.timer = setTimeout(s._delay,500);
      }
    }
    window.addEventListener('resize',function(){
      s.current = 0;
      s.reset();
      s.pos();
      s.rtime = new Date();
      s.action = true;
      clearTimeout(s.timer);
      if (s.timeout === false) {
        s.timeout = true;
        setTimeout(s.resizeend, s.delta);
      }
    })
    this.btnPrev = document.getElementById('reno_ctl_btn_prev');
    this.btnNext = document.getElementById('reno_ctl_btn_next');
    this.btnNext.addEventListener('click', function() {
      if (s.action == false) {
        window.cancelAnimFrame(s.delay);
        if((parseInt(s.current)+1) > s._length - 1) {
          s.handling_prev(0);
        } else {
          s.handling_next(parseInt(s.current)+1);
        }
      } else {
        return;
      }
    });
    /** Next Slider **/
    this.btnPrev.addEventListener('click', function() {
      if (s.action == false) {
        window.cancelAnimFrame(s.delay);
        if((parseInt(s.current)-1) < 0) {
          s.handling_next(s._length - 1);
        } else {
          s.handling_prev(parseInt(s.current)-1);
        }
      } else {
        return;
      }
    });
    this._delay();
    this.pos();
  }
  return Slide
})()
window.addEventListener('DOMContentLoaded',function(){
  new Effect();
  new Slide();
  new Anchor();
})
