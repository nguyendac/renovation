window.addEventListener('DOMContentLoaded', function() {
  // new Sticky();
  // new ExtraNav();
  // new MenuSp();
});
var ExtraNav = (function() {
  function ExtraNav() {
    var e = this;
    this.clone = document.getElementById('social').cloneNode(true);
    this.clone.removeAttribute('id');
    this.clone.classList.remove('show_pc');
    this.clone.classList.add('show_sp');
    this.des = document.getElementById('nav');
    this.des.appendChild(this.clone);
  }
  return ExtraNav;
})()
var MenuSp = (function() {
  function MenuSp() {
    var m = this;
    this._target = document.getElementById('icon_nav');
    this._mobile = document.getElementById('nav');
    this._header = document.getElementById('header');
    this._target.addEventListener('click', function() {
      if (this.classList.contains('open')) {
        this.classList.remove('open');
        m._mobile.classList.remove('open');
        m._mobile.style.height = 0;
        document.body.style.overflow = 'inherit';
        document.getElementById('header').style.background = ('transparent');
      } else {
        this.classList.add('open');
        m._mobile.classList.add('open');
        document.body.style.overflow = 'hidden';
        document.getElementById('header').style.background = ('rgba(0,0,0,0.9)');
        var h = 0;
        Array.prototype.forEach.call(m._mobile.children, function(el) {
          h += el.clientHeight;
        })
        if(h > (window.innerHeight - m._header.clientHeight)) {
          h = window.innerHeight - m._header.clientHeight;
        } else {
          h = h;
        }
        m._mobile.style.height = h + 'px';
      }
    })
    this._reset = function() {
      if (m._target.classList.contains('open')) {
        if (window.innerWidth > 769) {
          m._target.classList.remove('open');
          m._mobile.classList.remove('open');
          document.body.removeAttribute('style');
          m._mobile.style.height = 'auto';
          m._mobile.style.top = 0;
          document.getElementById('header').style.background = ('transparent');
        } else {
          var h = 0;
          Array.prototype.forEach.call(m._mobile.children, function(el) {
            h += el.clientHeight;
          })
          if(h > (window.innerHeight - m._header.clientHeight)) {
            h = window.innerHeight - m._header.clientHeight;
          } else {
            h = h;
          }
          m._mobile.style.height = h + 'px';

        }
      } else {
        if (window.innerWidth < 769) {
          m._mobile.style.height = 0;
          m._mobile.style.top = closest(m._target, 'header').clientHeight + 'px';;
        } else {m
          m._mobile.style.height = 'auto';
        }
      }
    }
    m._reset();
    window.addEventListener('resize', m._reset, false);
  }
  return MenuSp;
})()
var Sticky = (function(){
  function Sticky(){
    var s = this;
    this._target = document.getElementById('header');
    this._mobile = document.getElementById('nav');
    this._for_sp = function(top){
      s._target.style.left = 0;
      s._mobile.style.top = s._target.clientHeight+'px';
      if(top > 0) {
        s._target.classList.add('fixed');
      } else {
        s._target.classList.remove('fixed');
      }
    }
    this._for_pc = function(top,left){
      if(top > 0) {
        s._target.classList.add('fixed');
        document.body.style.paddingTop = 0;
        s._target.style.left = -left+'px';
      } else {
        s._target.classList.remove('fixed');
      }
    }
    this.handling = function(){
      var _top  = document.documentElement.scrollTop || document.body.scrollTop;
      var _left  = document.documentElement.scrollLeft || document.body.scrollLeft;
      if(window.innerWidth < 769) {
        s._for_sp(_top);
      }  else {
        if(!s._target.classList.contains('top')) {
          s._target.classList.remove('fixed')
        }
        s._for_pc(_top,_left);
      }
    }
    window.addEventListener('scroll',s.handling,false);
    window.addEventListener('resize',s.handling,false);
    window.addEventListener('load',s.handling,false);
  }
  return Sticky;
})()